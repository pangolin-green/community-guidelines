# Community Guidelines

1. Membership in the community, including access to buildings and resources within the community, shall be contingent upon compliance with these guidelines.
2. Community members are expected to conduct themselves in such a way as not to harm the community.
3. Members are expected to maintain sustainability within the community. Unsustainable activites are not permitted, including but not limited to: burning of petroleum or diesel fuels, reckless creation of air, water, or soil pollution, excessive consumption of natural resources, or the reckless creation of waste.
    1. Each member shall be responsible for their own waste. Should a member bring material into the community which becomes waste, that member must renew, recycle, repurpose, or remove the waste from the community.
3. To maintain residence in the community, members reside within the community, excepting intervals of no longer than 30 days, for no fewer than 245 days each calendar year. Should these requirements not be met, a member shall forfeit residence and be required to re-apply before again residing in the community, subject to approval.
4. Members may host visiting guests for a period not to exceed 7 contiguous days or 60 total days per calendar year, exempting approved exceptions by the Board or its Officers.
    1. Guests shall be required to abide by the Community Guidelines.
    2. Guests shall not leave any waste behind in the community.
    3. Hosts shall be held responsible for their guests and their guests' waste.
5. From time to time, members may elect administrators to bear particular responsibilities within the community by unanimous vote. At any time a vote may be called to recall an administrator by 2/3 majority vote.
6. Members may voluntarily forfeit residence at any time. A member's residence in the community may be revoked by 2/3 majority vote of the other members or by decision of the Board of Directors, observing all applicable law concerning evictions.
7. Members shall not violate any law or regulation within the jurisdiction in which the community resides.
8. Members shall not construct any building or structure except as approved by the Board or its Officers.

These guidelines may be updated as needed by the Board of Directors or President of The Pangolin Green Foundation, with 7 days notice before changes shall take effect.
